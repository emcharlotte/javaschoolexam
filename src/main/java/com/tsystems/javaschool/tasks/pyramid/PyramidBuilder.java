package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.size() > 100) {
            throw new CannotBuildPyramidException();
        }

        int k = inputNumbers.size();
        for (int i = 0; i <= inputNumbers.size() / 2; i++) {
            if (k - i > 0) {
                k -= i;
            } else {
                break;
            }
        }
        Collections.sort(inputNumbers);
        int[][] result = new int[k][k + k - 1];
        int z = 0;
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < i + 1; j++) {
                result[i][j] = inputNumbers.get(z);
                z++;
            }
        }
        int shift = result[0].length / 2;
        for (int a = 0; a < result.length; a++) {
            int[] temp = new int[result[0].length];
            int w = 1;
            for (int i = 0; i < result[0].length; i++) {
                if (i + shift + w < result[0].length) {
                    if (i == 0) {
                        temp[i + shift] = result[a][i];
                    } else {
                        temp[i + shift + w] = result[a][i];
                        w++;
                    }
                }
            }
            result[a] = temp;
            System.out.println(Arrays.toString(result[a]));
            shift--;
        }
        return result;
    }
}
