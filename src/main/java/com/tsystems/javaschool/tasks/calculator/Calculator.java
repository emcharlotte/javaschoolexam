package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
       
        if (statement == null || statement == "" || statement.contains("..") || statement.contains("++") ||
                statement.contains("--") || statement.contains("**") || statement.contains("//")) {
            return null;
        }

        LinkedList<Character> check = new LinkedList<>();
        for (Character ch : statement.toCharArray()) {
            if (ch == '(') {
                check.add(ch);
            }
            if ((ch == ')') && check.size() == 0) {
                check.add('e');
            } else if (ch == ')') {
                if (check.getLast() == '(') {
                    check.removeLast();
                }

            }
        }
        if (check.size() != 0) {
            return null;
        }

        double res = eval(statement);
        if (String.valueOf(res).equals("0.0") || String.valueOf(res).equals("Infinity")) {
            return null;
        } else if (String.valueOf(res).contains(".0")) {
            return String.valueOf((int) res);
        } else return String.valueOf(res);
    }

    public static double eval(String str) {
        return new Object() {
            int pos = -1;
            int ch;

            void nextChar() {
                ch = (++pos < str.length()) ? str.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    nextChar();
                    return true;
                }
                return false;
            }

            double parse() {
                nextChar();
                double x = parseExpression();
                if (pos < str.length()) {
                    return 0.0;
                }
                return x;
            }


            double parseExpression() {
                double x = parseTerm();
                for (; ; ) {
                    if (eat('+')) {
                        x += parseTerm();
                    } else if (eat('-')) {
                        x -= parseTerm();
                    } else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (; ; ) {
                    if (eat('*')) {
                        x *= parseFactor();
                    } else if (eat('/')) {
                        x /= parseFactor();
                    } else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor();
                if (eat('-')) return -parseFactor();

                double x;
                int startPos = this.pos;
                if (eat('(')) {
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') {
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(str.substring(startPos, this.pos));
                } else {
                    throw new RuntimeException("Unexpected: " + (char) ch);
                }

                return x;
            }
        }.parse();
    }

}
