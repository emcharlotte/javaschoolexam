package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }
        if (x.equals(new ArrayList())) {
            return true;
        }

        int k = 0;
        for (int i = 0; i < x.size(); i++) {
            for (int f = k; f < y.size(); f++) {
                if (x.get(i).equals(y.get(f))) {
                    k++;
                    break;
                } else {
                    y.set(f, null);
                    k++;
                }
            }
        }
        List yy = y;
        for (int a = 0; a < yy.size(); a++) {
            if (yy.get(a) == null) {
                y.remove(a);
                a--;
            }
        }
        for (int b = x.size(); b < y.size(); b++) {
            y.remove(b);
        }

        if (y.equals(x)) {
            return true;
        }
        return false;
    }
}
